/**
 * Created by User on 06.10.2017.
 */
public class Settings {
    private static String homeUrl;


    public static final String PATH_GECKODRIVER= "drivers/geckodriver.exe";
    //public static final int WAIT_TO_ELEMENT_SECONDS = 10;

    public static String getHomeUrl() {
        return homeUrl;
    }

    public static void setHomeUrl(String homeUrl) {
        Settings.homeUrl = homeUrl;
    }
}
