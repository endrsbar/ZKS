
import org.apache.commons.logging.Log;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

    //Be careful in splitting the test code to individual @Tests:
//an execution plan of Tests is not guaranteed by default
//We need to use @FixMethodOrder(MethodSorters.{NAME_ASCENDING}) above unit test class declaration
//http://junit.org/apidocs/index.html?org/junit/runners/MethodSorters.html
    @FixMethodOrder(MethodSorters.NAME_ASCENDING)
    public class TestProstredi {

        static RemoteWebDriver driver;

        @BeforeClass
        public static void beforeClass() {
            // System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
            // driver = new ChromeDriver();
            System.setProperty("webdriver.gecko.driver", Settings.PATH_GECKODRIVER);
            Settings.setHomeUrl("http://demo.redmine.org/");
            driver = new FirefoxDriver();
        }


        @Test
        public void step1_loginTest() {

            driver.get(Settings.getHomeUrl());
            WebDriverWait wait = new WebDriverWait(driver, 10);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.className("login")));
            element.click();
            // driver.findElement(By.className("login")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated( By.id("username")));

            driver.findElement(By.id("username")).sendKeys("endrbar");
            driver.findElement(By.id("password")).sendKeys("hesloredmine");
            driver.findElement(By.xpath("//*[@id=\"login-form\"]/form/table/tbody/tr[4]/td[2]/input")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated( By.id("loggedas")));
        }

        @Test
        public void step2_projectTest() {
            // FIND TESTING PROJECT
            driver.findElement(By.className("projects")).click();

            WebDriverWait wait = new WebDriverWait(driver, 10);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Test project")));

            element.click();

            // CHECK NUMBER OF BUGS - has to be less than 20
            WebElement bugsLinkElement = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Bug")));
            WebElement bugsStatsElement = bugsLinkElement.findElement(By.xpath("parent::node()"));
            String bugsStatusString = bugsStatsElement.getText();
            String totalBugsCount = bugsStatusString.substring(bugsStatusString.indexOf("/") + 1).trim();
            int totalBugsCountNumber = (new Integer(totalBugsCount)).intValue();

            // JUnit style assert
            assertTrue("Log message: totalBugsCountNumber is more than 20", totalBugsCountNumber <= 20);

            //fail();
        }

        // try to set alwaysRun false and fail previous test
        //@Test (dependsOnMethods={"projectTest"})
        @Test
        public void step3_logoutTest() {
            // LOG OUT
            driver.findElement(By.className("logout")).click();
        }

        /*
        * pri nenalezeni chyboveho hlseni vyhodi: NoSuchElementException: Cannot locate an element using By.id: flash_error
        * */
        @Test
        public void step4_wrongLoginTest() {
            driver.get(Settings.getHomeUrl());
            WebDriverWait wait = new WebDriverWait(driver, 10);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.className("login")));
            element.click();

            wait.until(ExpectedConditions.presenceOfElementLocated( By.id("username")));

            driver.findElement(By.id("username")).sendKeys("endrsbar");
            driver.findElement(By.id("password")).sendKeys("blbost");
            driver.findElement(By.xpath("//*[@id=\"login-form\"]/form/table/tbody/tr[4]/td[2]/input")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("flash_error")));
        }

        @AfterClass
        public static void afterClass() {
            //Pause.pause(2);
            driver.quit();
        }

    }

