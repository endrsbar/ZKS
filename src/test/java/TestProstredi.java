
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.LoginPageObject;
import pageObjects.ProjectElement;
import pageObjects.ProjectPageObject;


import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

    //Be careful in splitting the test code to individual @Tests:
//an execution plan of Tests is not guaranteed by default
//We need to use @FixMethodOrder(MethodSorters.{NAME_ASCENDING}) above unit test class declaration
//http://junit.org/apidocs/index.html?org/junit/runners/MethodSorters.html
    @FixMethodOrder(MethodSorters.NAME_ASCENDING)
    public class TestProstredi {

        static RemoteWebDriver driver;
        static LoginPageObject loginPageObject;
        static ProjectPageObject projectPageObject;

        @BeforeClass
        public static void beforeClass() {
            // System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
            // driver = new ChromeDriver();
            System.setProperty("webdriver.gecko.driver", Settings.PATH_GECKODRIVER);
            Settings.setHomeUrl("http://demo.redmine.org/");
            driver = new FirefoxDriver();
            loginPageObject = new LoginPageObject(Settings.getHomeUrl(),driver);
            projectPageObject= new ProjectPageObject(Settings.getHomeUrl(),driver);
        }


        @Test
        public void step1_loginTest() throws InterruptedException {

           // driver.get(Settings.getHomeUrl());
            loginPageObject.open();
            loginPageObject.login("endrsbar", "hesloredmine");
            loginPageObject.checkGoodLogin();
        }



        // try to set alwaysRun false and fail previous test
        //@Test (dependsOnMethods={"projectTest"})
        @Test
        public void step3_logoutTest() {
            // LOG OUT
            loginPageObject.logout();
        }

        @Test
        public void step2_projectTest() throws InterruptedException {
            // FIND TESTING PROJECT
            projectPageObject.open();
            projectPageObject.getProjectElements();
            ProjectElement projectElement = projectPageObject.item(2);// najdu element s projektem na pozici
            assertEquals("00a40891c6c38654d657", projectElement.getProjectName());
            projectElement.goToElement(); // nebo open() prejde na stranku projektu

            Thread.sleep(3000);

            assertEquals("00a40891c6c38654d657", driver.findElement(By.xpath("//*[@id=\"header\"]/h1")).getText());

        }
        /*
        * pri nenalezeni chyboveho hlseni vyhodi: NoSuchElementException: Cannot locate an element using By.id: flash_error
        */
        @Test
        public void step4_wrongLoginTest() throws InterruptedException {
            loginPageObject.open();
            loginPageObject.login("endrsbar","blbost");
            loginPageObject.checkWrongLogin();
        }

        @AfterClass
        public static void afterClass() {
            //Pause.pause(2);
            driver.quit();
        }

    }

