import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author Karel
 */
public class SampleTest {

    private static FirefoxDriver driver;
    private static String baseUrl;
    private StringBuffer verificationErrors = new StringBuffer();

    public SampleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // https://github.com/mozilla/geckodriver
        String s = System.setProperty("webdriver.gecko.driver", Settings.PATH_GECKODRIVER);
        driver = new FirefoxDriver();
        Settings.setHomeUrl("http://demo.redmine.com/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }


    @Test
    public void hello() {
        driver.get(Settings.getHomeUrl());
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @After
    public void tearDown() {
        driver.quit();
        if (verificationErrors.length() > 0) {
            fail(verificationErrors.toString());
        }
    }
}

