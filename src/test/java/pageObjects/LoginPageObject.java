package pageObjects;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObject {
    private final String baseUrl;

    private final WebDriver driver;

    public LoginPageObject(String baseUrl, WebDriver driver) {
        this.baseUrl = baseUrl;
        this.driver = driver;
    }

    public void logout(){
        driver.findElement(By.className("logout")).click();
    }

    public void open() throws InterruptedException {
        driver.get(baseUrl + "login");
        Thread.sleep(3000);
    }

    public void checkWrongLogin(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("flash_error")));
    }

    public void checkGoodLogin(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated( By.id("loggedas")));
    }

    public void setPassword(String password) {
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.id("password")).sendKeys(password);
    }

    public void setUserName(String userName) {
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.id("username")).sendKeys(userName);
    }

   // public void setPermLogin() {
   //     driver.findElement(By.name("perm_login")).click();
    //}

    public void login() {
        driver.findElement(By.xpath("//*[@id=\"login-form\"]/form/table/tbody/tr[4]/td[2]/input")).click();
    }

    public void login(String userName, String password) {
        setUserName(userName);
        setPassword(password);
        driver.findElement(By.xpath("//*[@id=\"login-form\"]/form/table/tbody/tr[4]/td[2]/input")).click();
    }
}
