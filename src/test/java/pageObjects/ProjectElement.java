package pageObjects;

import com.sun.scenario.Settings;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.security.Key;
import java.util.List;

public class ProjectElement {


        private WebDriver driver;
        private WebElement webElement;

        public ProjectElement(WebElement element) {
            webElement = element;
        }

        public String getProjectName() {
            return webElement.getText();
        }

        public void goToElement(){
            webElement.click();
        }



}
