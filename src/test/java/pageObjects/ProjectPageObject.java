package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;

import java.util.ArrayList;
import java.util.List;



public class ProjectPageObject {

    private final String baseUrl;
    private final WebDriver driver;
    public static List<WebElement> we;

    public ProjectPageObject(String baseUrl, WebDriver driver) {
        this.baseUrl = baseUrl;
        this.driver = driver;
    }

    public void open() throws InterruptedException {
        driver.get(baseUrl + "projects");
        Thread.sleep(3000);
    }

    public List<WebElement> getProjectElements() {
        WebElement table = driver.findElement(By.id("projects-index"));
        we = ((RemoteWebElement)table).findElementsByClassName("root");
        return we;
    }

    public ProjectElement item(int index){
        return new ProjectElement(we.get(index));
    }

    public WebElement getItem(int i) {
        getProjectElements();
        return we.get(i);
    }

    public String getItemName(int i) {
        return getItem(i).getText();
    }

    public String getH1(){
        return driver.findElement(By.xpath("//*[@id=\"header\"]/h1")).getText();

    }

    public void goToProject(int i){
        getItem(i).click();
    }

}

/*
ProjectElement projectElement = projectListPage.getProjectElements().item(2);
            assertEquals("ADRIN project management", projectElement.getProjectName());
            projectElement.gotoProjectPage(); // nebo open() prejde na stranku projektu

*/


